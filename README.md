# Loading ASCII BAR (Ampliación)

En esta práctica seguiremos avanzando en el uso de la función `printf` junto las estructuras de control de flujo.
Para ello crearemos una barra de progreso en código ASCII que emulará la realización de una tarea por parte del computador 
informándonos de su grado de realización. 
El siguiente ASCII cast, te ayudará a ver el resultado que esperamos obtener.

[![asciicast](https://asciinema.org/a/CQCGSvecKMlmzvMO3Zry04QcR.svg)](https://asciinema.org/a/CQCGSvecKMlmzvMO3Zry04QcR)

Para su realización deberás tener en cuenta:

1. La función `printf` te permite especificar el tamaño mínimo que tendrá el que se va a mostrar, de forma que si su longitud es menor 
   se rellenará con carácteres vacios o 0's según sea un dato numérico o dato tipo texto.
2. La función `printf` permite definir la alineación de los datos a mostrar
3. El carácter del control `\r` nos permite volver al inicio de la línea, de este modo podemos reescribirla y conseguir ese
   efecto de actualización de la pantalla que necesitamos.
   El [siguiente enlace](https://www.geeksforgeeks.org/format-specifiers-in-java/) nos proporciona ejemplos de uso 
   de la función printf
4. Haremos uso del siguiente snippet de código en cada iteración para hacer que la ejecución de nuestro script espere 200ms.
   
    ```java
            try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
    ```
    ***De momento no necesitamos comprender su funcionamiento, simplemente lo copiaremos y pegaremos donde necesitamos que 
    el código espere durante 200ms***
      
 


